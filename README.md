# Abstract Tent

Tent is a service used to provide Alpine Repository images. In a nutshell, it's our Alpine Repository image.

## Environment Vars

TENT_URI: Domain for tent to listen on.
TENT_REPO_DIR: Directory where repo data is stored.
